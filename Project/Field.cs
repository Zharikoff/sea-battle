﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Project
{
    // W - water
    // O - occupied
    // S - ship
    // E - error
    // X - shot
    // 0 - miss
    // Z - destroyed
    class Field
    {
        private char[,] field = new char[10, 10];
        internal int[] ships = new int[4] { 4,3,2,1};
        internal List<string[]> shipIndex = new List<string[]>();
        internal bool player;

        public Field(bool player = true)
        {
            this.player = player;
        }

        public char this[char letter, int index]
        {
            get
            {
                index--;
                int charIndex = (int)letter.ToString().ToUpper()[0] - 65;
                if (
                       index > this.field.Length
                    || charIndex > this.field.Length
                    || index < 0
                    || charIndex < 0
                    )
                {
                    Console.WriteLine("Index was out of bounds");
                    return 'E';
                }
                else
                {
                    return this.field[index, charIndex];
                }
            }
            set
            {
                index--;
                int charIndex = (int)letter.ToString().ToUpper()[0] - 65;
                if (
                       charIndex > this.field.Length
                    || index > this.field.Length
                    || charIndex < 0
                    || index < 0
                    )
                {
                    Console.WriteLine("Index was out of bounds");
                }
                else
                {
                    this.field[index, charIndex] = value;
                }
            }
        }

        public void Init()
        {
            for (char i = 'a'; i <= 'j'; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    this[i, j] = 'W';
                }
            }
        }
        public void Place(char letter, int index, char letter2, int index2, int size)
        {
            letter = letter.ToString().ToLower()[0];
            letter2 = letter2.ToString().ToLower()[0];
            if (ships[size - 1] == 0)
            {
                Console.WriteLine("You don't have such ship(s) left.");
                return;
            }
            string msg = "You cant place ship here.";
            int charIndex1 = (int)letter.ToString().ToUpper()[0] - 65;
            int charIndex2 = (int)letter2.ToString().ToUpper()[0] - 65;
            if (letter == letter2 && index == index2 && size == 1)
            {
                if (this[letter, index] == 'O' || this[letter, index] == 'S')
                {
                    if (player)
                    {
                        Console.WriteLine(msg);
                    }
                    return;
                }

                shipIndex.Add(new string[4] { letter.ToString(), index.ToString(), letter2.ToString(), index2.ToString()});
                ships[size - 1]--;
                this[letter, index] = 'S';
                if (index > 1)
                {
                    this[letter, index - 1] = 'O';
                }
                if (index2 < 10)
                {
                    this[letter, index + 1] = 'O';
                }
                if (letter > 'a')
                {
                    this[(char)(letter - 1), index] = 'O';
                    if (index > 1)
                    {
                        this[(char)(letter - 1), index - 1] = 'O';
                    }
                    if (index2 < 10)
                    {
                        this[(char)(letter - 1), index + 1] = 'O';
                    }
                }
                if (letter < 'j')
                {
                    this[(char)(letter2 + 1), index2] = 'O';
                    if (index > 1)
                    {
                        this[(char)(letter2 + 1), index2 - 1] = 'O';
                    }
                    if (index2 < 10)
                    {
                        this[(char)(letter2 + 1), index2 + 1] = 'O';
                    }
                }
            }
            else if (charIndex1 == charIndex2 && index2 - index == size - 1)
            {
                for (int i = index; i <= index2; i++)
                {
                    if (this[letter, i] == 'O' || this[letter, i] == 'S')
                    {
                        if (player)
                        {
                            Console.WriteLine(msg);
                        }
                        return;
                    }
                }

                shipIndex.Add(new string[4] { letter.ToString(), index.ToString(), letter2.ToString(), index2.ToString() });
                ships[size - 1]--;
                for (int i = index; i <= index2; i++)
                {
                    this[letter, i] = 'S';
                    if (letter > 'a')
                    {
                        this[(char)(letter - 1), i] = 'O';
                    }
                    if (letter2 < 'j')
                    {
                        this[(char)(letter + 1), i] = 'O';
                    }
                }
                if (index > 1)
                {
                    this[letter, index - 1] = 'O';
                    if (letter > 'a')
                    {
                        this[(char)(letter - 1), index - 1] = 'O';
                    }
                    if (letter2 < 'j')
                    {
                        this[(char)(letter + 1), index - 1] = 'O';
                    }
                }
                if (index2 < 10)
                {
                    this[letter2, index2 + 1] = 'O';
                    if (letter > 'a')
                    {
                        this[(char)(letter - 1), index2 + 1] = 'O';
                    }
                    if (letter2 < 'j')
                    {
                        this[(char)(letter + 1), index2 + 1] = 'O';
                    }
                }
            }
            else if (index == index2 && charIndex2 - charIndex1 == size - 1)
            {
                for (char i = letter; i <= letter2; i++)
                {
                    if (this[i, index] == 'O' || this[i, index] == 'S')
                    {
                        if (player)
                        {
                            Console.WriteLine(msg);
                        }
                        return;
                    }
                }

                shipIndex.Add(new string[4] { letter.ToString(), index.ToString(), letter2.ToString(), index2.ToString() });
                ships[size - 1]--;
                for (char i = letter; i <= letter2; i++)
                {
                    this[i, index] = 'S';
                    if (index > 1)
                    {
                        this[i, index - 1] = 'O';
                    }
                    if (index2 < 10)
                    {
                        this[i, index + 1] = 'O';
                    }
                }
                if (letter > 'a')
                {
                    this[(char)(letter - 1), index] = 'O';
                    if (index > 1)
                    {
                        this[(char)(letter - 1), index - 1] = 'O';
                    }
                    if (index2 < 10)
                    {
                        this[(char)(letter - 1), index + 1] = 'O';
                    }
                }
                if (letter2 < 'j')
                {
                    this[(char)(letter2 + 1), index2] = 'O';
                    if (index > 1)
                    {
                        this[(char)(letter2 + 1), index2 - 1] = 'O';
                    }
                    if (index2 < 10)
                    {
                        this[(char)(letter2 + 1), index2 + 1] = 'O';
                    }
                }
            }
            else
            {
                if (player)
                {
                    Console.WriteLine(msg);
                }
            }
        }
        public void Print(bool gameStarted = false)
        {
            char[,] outField = field;
            Console.WriteLine(" a b c d e f g h i j");
            for (int i = 1; i <= 10; i++)
            {
                for (char j = 'a'; j <= 'j'; j++)
                {
                    if (this[j, i] == 'O' && gameStarted)
                    {
                        this[j, i] = 'W';
                    }
                    if (this[j, i] == 'W')
                    {
                        Console.ForegroundColor = ConsoleColor.DarkBlue;
                    }
                    else if (this[j, i] == 'O')
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                    }
                    else if (this[j, i] == 'X' || this[j, i] == 'Z')
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                    else if (this[j, i] == '0')
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    Console.Write($" {this[j, i]}");
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write($" {i}");
                Console.WriteLine();
            }
        }
        public void Random()
        {
            this.player = false;
            Random rnd = new Random();
            while (this.ships[3] != 0)
            {
                char letter = (char)(rnd.Next(6) + 65);
                char letter2 = (char)(letter + 3);
                int index = rnd.Next(1, 7);
                bool isLetter = true;
                if (isLetter)
                {
                    this.Place(letter, index, letter2, index, 4);
                }
                else
                {
                    this.Place(letter, index, letter, index + 3, 4);
                }
            }
            while (this.ships[2] != 0)
            {
                char letter = (char)(rnd.Next(7) + 65);
                char letter2 = (char)(letter + 2);
                int index = rnd.Next(1, 8);
                bool isLetter = Convert.ToBoolean(rnd.Next(2));
                if (isLetter)
                {
                    this.Place(letter, index, letter2, index, 3);
                }
                else
                {
                    this.Place(letter, index, letter, index + 2, 3);
                }
            }
            while (this.ships[1] != 0)
            {
                char letter = (char)(rnd.Next(8) + 65);
                char letter2 = (char)(letter + 1);
                int index = rnd.Next(1, 9);
                bool isLetter = Convert.ToBoolean(rnd.Next(2));
                if (isLetter)
                {
                    this.Place(letter, index, letter2, index, 2);
                }
                else
                {
                    this.Place(letter, index, letter, index + 1, 2);
                }
            }
            while (this.ships[0] != 0)
            {
                char letter = (char)(rnd.Next(9) + 65);
                int index = rnd.Next(1, 10);
                this.Place(letter, index, letter, index, 1);
            }
        }
        public void Fill()
        {
            while (ships[3] != 0 || ships[2] != 0 || ships[1] != 0 || ships[0] != 0)
            {
                this.Print();
                Regex point = new Regex(@"([a-zA-Z]+)(\d+)");
                Match pointMatch;
                Match pointMatch2;
                Console.WriteLine("Enter ship size");
                string input = Console.ReadLine();
                int size;
                if (int.TryParse(input, out int num))
                {
                    if (num > 4 || num < 1)
                    {
                        Console.WriteLine("Wrong size has been entered (1-4)");
                        continue;
                    }
                    else
                    {
                        if (ships[num - 1] == 0)
                        {
                            Console.Clear();
                            Console.WriteLine("You don't have such ship(s) left");
                            continue;
                        }
                        size = num;
                    }
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Please use correct numbers");
                    continue;
                }
                int index;
                int index2;
                char letter;
                char letter2;
                bool verified = true;
                string ltrmsg = "Please enter a letter from range 'a' to 'j'";
                string indmsg = "Please enter a number from range 1 to 10";
                do
                {
                    Console.WriteLine("Enter first point (B2 or c7)");
                    input = Console.ReadLine();
                    pointMatch = point.Match(input);
                    if (size != 1)
                    {
                        Console.WriteLine("Enter second point (b5 or F7)");
                        input = Console.ReadLine();
                    }
                    pointMatch2 = point.Match(input);
                    letter = pointMatch.Groups[1].Value.ToLower()[0];
                    letter2 = pointMatch2.Groups[1].Value.ToLower()[0];
                    if (letter < 'a' || letter > 'j' || letter2 < 'a' || letter2 > 'j')
                    {
                        Console.WriteLine(ltrmsg);
                        verified = false;
                    }
                    index = Convert.ToInt32(pointMatch.Groups[2].Value);
                    index2 = Convert.ToInt32(pointMatch2.Groups[2].Value);
                    if (index < 1 || index > 10 || index2 < 1 || index2 > 10)
                    {
                        Console.WriteLine(indmsg);
                        verified = false;
                    }
                } while (!verified);
                Console.Clear();
                this.Place(letter, index, letter2, index2, size);
            }
        }
        public void Update()
        {
            Console.Clear();
            this.Print(true);
        }
        public void Attacked(char letter, int index, out bool hit)
        {
            if (this[letter, index] == 'S')
            {
                if (player)
                {
                    Console.WriteLine("Target damaged");
                }
                this[letter, index] = 'X';
                hit = true;
            }
            else
            {
                if (this[letter, index] != 'X' && this[letter, index] != 'Z')
                {
                    this[letter, index] = '0';
                }
                hit = false;
            }
        }
        public bool HaveLost()
        {
            for(char i = 'a'; i <= 'j'; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    if (this[i, j] == 'S')
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public void DestroyedCheck()
        {
            foreach (var range in shipIndex)
            {
                bool destroyed = true;
                int ind = Convert.ToInt32(range[1]);
                int ind2 = Convert.ToInt32(range[3]);
                char ch = range[0][0];
                char ch2 = range[2][0];
                if (ch == ch2)
                {
                    for (int index = ind; index <= ind2; index++)
                    {
                        if (this[ch, index] != 'X')
                        {
                            destroyed = false;
                        }
                    }
                    if (destroyed)
                    {
                        for (int index = ind; index <= ind2; index++)
                        {
                            this[ch, index] = 'Z';
                        }
                    }
                }
                if ( ind == ind2)
                {
                    for (char letter = ch; letter <= ch2; letter++)
                    {
                        if (this[letter, ind] != 'X')
                        {
                            destroyed = false;
                        }
                    }
                    if (destroyed)
                    {
                        for (char letter = ch; letter <= ch2; letter++)
                        {
                            this[letter, ind] = 'Z';
                        }
                    }
                }
            }
        }
    }
}

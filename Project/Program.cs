﻿using System;

namespace Project
{
    class Program
    {
        static void Main(string[] args)
        {
            GameLogic.Introduction();
            GameLogic.Start();
            Console.Read();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Project
{
    static class GameLogic
    {
        public static void Introduction()
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Zharikoff's Sea Battle!");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Markers:");
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("W - Water");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("O - Occupied (You can't place ships there)");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("S - Ship (Can't place ship here either)");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("X - Hit target");
            Console.WriteLine("Z - Target destroyed");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("0 - Missed shot");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }
        public static void Start()
        {
            Field player = new Field();
            Field enemy = new Field(false);
            Field fog = new Field(false);

            string name;

            player.Init();
            enemy.Init();
            fog.Init();

            Console.WriteLine("Enter player name");
            name = Console.ReadLine();
            while (true) {
                Console.Clear();
                Console.WriteLine("Do you want to fill your field\n" +
                "1. Automatically\n" +
                "2. Custom");
                string result = Console.ReadLine();
                if (result == "1" || result == "Automatically")
                {
                    player.Random();
                    player.player = true;
                    break;
                }
                if (result == "2" || result == "Custom")
                {
                    player.Fill();
                    break;
                }
                else
                {
                    Console.WriteLine("Enter a valid option");
                    continue;
                }
            }
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Do you want to fill enemy field\n" +
                "1. Automatically\n" +
                "2. Custom");
                string result = Console.ReadLine();
                if (result == "1" || result == "Automatically")
                {
                    enemy.Random();
                    break;
                }
                if (result == "2" || result == "Custom")
                {
                    enemy.Fill();
                    break;
                }
                else
                {
                    Console.WriteLine("Enter a valid option");
                    continue;
                }
            }

            AttackPhase(name, player, enemy, fog);
        }
        public static void AttackPhase(string playerName, Field player, Field enemy, Field fog)
        {
            Console.WriteLine("Attack phase.");
            fog.shipIndex = enemy.shipIndex;
            fog.Print();
            Console.WriteLine("Enemy field");
            player.Print(true);
            Console.WriteLine("Your field");
            Regex point = new Regex(@"([a-zA-Z]+)(\d+)");
            Match pointMatch;
            string input;
            int index;
            char letter;
            string ltrmsg = "Please enter a letter from range 'a' to 'j'";
            string indmsg = "Please enter a number from range 1 to 10";
            while (true)
            {
                if (enemy.HaveLost())
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine($"{playerName} has won.");
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                }
                if (player.HaveLost())
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("The enemy has won.");
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                }
                Console.WriteLine("Enter a cell to attack (A1)");
                input = Console.ReadLine();
                pointMatch = point.Match(input);
                if (pointMatch.Success)
                {
                    letter = pointMatch.Groups[1].Value.ToLower()[0];
                    if (letter < 'a' || letter > 'j')
                    {
                        Console.WriteLine(ltrmsg);
                        continue;
                    }

                    index = Convert.ToInt32(pointMatch.Groups[2].Value);
                    if (index < 1 || index > 10)
                    {
                        Console.WriteLine(indmsg);
                        continue;
                    }
                }
                else
                {
                    Console.WriteLine("Wrong cell has been entered");
                    continue;
                }

                Console.Clear();
                enemy.Attacked(letter, index, out bool hit);
                if (hit)
                {
                    if (enemy[letter, index] == 'Z')
                    {
                        fog[letter, index] = 'Z';
                    }
                    else
                    {
                        fog[letter, index] = 'X';
                    }
                }
                else
                {
                    if (fog[letter, index] != 'X' && fog[letter, index] != 'Z')
                    {
                        fog[letter, index] = '0';
                    }
                }
                fog.DestroyedCheck();
                fog.Print(true);
                Console.WriteLine("Enemy field");
                if (!hit)
                {
                    bool enemyHit;
                    do
                    {
                        RandomAttack(player, out bool isHit);
                        player.DestroyedCheck();
                        enemyHit = isHit;
                    } while (enemyHit);
                }
                player.Print(true);
                Console.WriteLine("Your field");
            }

        }
        public static void RandomAttack(Field field, out bool isHit)
        {
            Random rnd = new Random();
            char letter = (char)(rnd.Next(9) + 65);
            int index = rnd.Next(1, 10);
            field.Attacked(letter, index, out bool hit);
            isHit = hit;
        }
    }
}
